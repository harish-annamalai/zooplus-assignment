# Rate Viewer Application

This is a Java + Thymeleaf based Spring boot application.
The Application allows for:

1. Logging into the Application using a username and password.
2. Once Logged in successfully, user will be redirected to landing page.
3. In the Landing page, the user will be able to select a List of Cryptos.
4. The user can optionally provide an I.P Address.
5. On Pressing the "Get Price" button, the user is provided the details of the Crypto Price.
6. A table is also populated with the Price History for the same Selected Crypto.
7. The Price and Date of the Shown Crypto is shown in the Locale of User's I.P address or that of the User's Region.

## Assumptions

1. The Application has dependency on I.P to Country Data.
2. With only I.P being provided, the I.P can be matched to country.
3. Mapping a country to Locale also needs a language, therefore, if an English Variant of the Locale is present, then it
   will be used.
4. For Price History, we would need historical data, which is already hosted on the Database.
5. The List of Supported Cryptos, their price, are normally provided in a Database. Therefore, Mocked data is loaded
   from a CSV and loaded at start up.

## Building the Application

The Application can be built using Standard maven commands.

```
mvnw -T 2.5C clean package
```

## Running the Application

The application can be run as Spring Boot Application using the following command.

### On Windows

```
set defaultUser=<desiered user name>
set defaultPassword=<desierd password>
set spring_profiles_active=loadData 
java -jar target\rate-viewer.jar
```

### On Linux/Mac

```
export defaultUser=<desiered user name>
export defaultPassword=<desierd password>
export spring_profiles_active=loadData 
java -jar target/rate-viewer.jar
```

## Hosting Details

The Code is hosted on git lab server at the following url
[Gitlab URL](https://gitlab.com/harish-annamalai/zooplus-assignment)

The Application is Hosted on Heroku, the application url
[Application URL](https://zooplus-assignment.herokuapp.com/)

## CI/CD

The CI/CD is configured to use the Free-Tier of GitLab and Heroku. GitLab is configured to run a Pipeline as soon as a
Push is performed on the Master Branch.

The Gitlab runs a maven image and perform `mvn clean verify` step to run the Compilation and JUnits.

Once the Gitlab maven step is complete, the Gitlab pushes the code to Heroku. The Heroku picks up the code and builds an
Image and Starts the application. 