package com.zooplus.rateviewer.utilities;

import javax.servlet.http.HttpServletRequest;

public class IPUtilities {

  private static final String[] IP_HEADERS = {
    "X-Forwarded-For",
    "Proxy-Client-IP",
    "WL-Proxy-Client-IP",
    "HTTP_X_FORWARDED_FOR",
    "HTTP_X_FORWARDED",
    "HTTP_X_CLUSTER_CLIENT_IP",
    "HTTP_CLIENT_IP",
    "HTTP_FORWARDED_FOR",
    "HTTP_FORWARDED",
    "HTTP_VIA",
    "REMOTE_ADDR"
  };

  private IPUtilities() {
    // Utility Class
  }

  public static long getIPInDecimalFormat(String ip) {
    long ipDecimalFormat = 0;
    String[] ipArr = ip.split("\\.");
    for (int i = 3; i >= 0; i--) {
      ipDecimalFormat += (Integer.parseInt(ipArr[3 - i]) * Math.pow(256, i));
    }
    return ipDecimalFormat;
  }

  public static String getIPAddressFromServletRequest(HttpServletRequest request) {

    for (String header : IP_HEADERS) {
      String ip = request.getHeader(header);
      if (ip != null && ip.length() != 0 && !"unknown".equalsIgnoreCase(ip)) {
        return ip;
      }
    }

    if (request.getRequestURL().toString().contains("localhost")) {
      return "127.0.0.1";
    }

    return request.getRemoteAddr();
  }
}
