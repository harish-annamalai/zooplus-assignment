package com.zooplus.rateviewer.services;

import com.zooplus.rateviewer.models.CurrencyType;
import com.zooplus.rateviewer.models.CurrentCurrencyRate;
import com.zooplus.rateviewer.models.PriceHistoryEntry;

import java.util.List;
import java.util.Optional;

public interface CurrencyServices {

  List<CurrencyType> getAllCurrencyTypes();

  Optional<CurrentCurrencyRate> getCurrentRateForCurrency(String code, String ip);

  List<PriceHistoryEntry> getPriceHistory(String code, String ip);
}
