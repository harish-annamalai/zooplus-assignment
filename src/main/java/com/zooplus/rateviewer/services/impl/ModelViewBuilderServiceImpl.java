package com.zooplus.rateviewer.services.impl;

import com.zooplus.rateviewer.models.CurrentCurrencyRate;
import com.zooplus.rateviewer.models.RateViewRequest;
import com.zooplus.rateviewer.services.CurrencyServices;
import com.zooplus.rateviewer.services.ModelViewBuilderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

@Service
public class ModelViewBuilderServiceImpl implements ModelViewBuilderService {

  @Autowired CurrencyServices services;

  @Override
  public ModelAndView getViewForCurrentRateForRequest(RateViewRequest rateViewRequest) {
    ModelAndView mav = new ModelAndView("index");
    String errors = this.validateRequest(rateViewRequest);
    if (errors.isEmpty()) {
      CurrentCurrencyRate currentRate =
          services
              .getCurrentRateForCurrency(rateViewRequest.getCode(), rateViewRequest.getIp())
              .orElse(null);
      if (currentRate == null) {
        errors = "Unable to get Price for Today";
      } else {
        mav.addObject("responseText", this.getCurrentRateDisplayMessage(currentRate));
      }
      mav.addObject(
          "priceHistory",
          services.getPriceHistory(rateViewRequest.getCode(), rateViewRequest.getIp()));
    }
    mav.addObject("errorText", errors);
    mav.addObject("currencies", services.getAllCurrencyTypes());
    mav.addObject("rateViewRequest", rateViewRequest);

    return mav;
  }

  @Override
  public ModelAndView getLandingView() {
    ModelAndView mav = new ModelAndView("index");
    mav.addObject("errorText", "");
    mav.addObject("currencies", services.getAllCurrencyTypes());
    mav.addObject("rateViewRequest", new RateViewRequest());
    return mav;
  }

  private String validateRequest(RateViewRequest request) {
    if (request.getCode().isEmpty()) {
      return "Please select a CryptoCurrency";
    }

    if (request.getIp().split("\\.").length != 4) {
      return "Please enter a valid I.P Number";
    }

    return "";
  }

  private String getCurrentRateDisplayMessage(CurrentCurrencyRate rate) {
    return "Price of "
        + rate.getName()
        + " with Code: "
        + rate.getCode()
        + " is: "
        + rate.getFormattedCurrencyValue();
  }
}
