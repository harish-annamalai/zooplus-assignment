package com.zooplus.rateviewer.services.impl;

import com.zooplus.rateviewer.entities.CurrencyHistory;
import com.zooplus.rateviewer.entities.CurrencyPriceKey;
import com.zooplus.rateviewer.models.CurrencyType;
import com.zooplus.rateviewer.models.CurrentCurrencyRate;
import com.zooplus.rateviewer.models.PriceHistoryEntry;
import com.zooplus.rateviewer.repositories.CurrenciesRepository;
import com.zooplus.rateviewer.repositories.CurrencyHistoryRepository;
import com.zooplus.rateviewer.services.CurrencyServices;
import com.zooplus.rateviewer.services.LocaleService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Log4j2
public class CurrencyServicesImpl implements CurrencyServices {

  private final Map<String, CurrencyType> currencyTypes = new HashMap<>();
  @Autowired CurrenciesRepository repository;
  @Autowired CurrencyHistoryRepository historyRepository;
  @Autowired LocaleService localeService;
  @Autowired CurrenciesRepository currenciesRepository;

  @Override
  public List<CurrencyType> getAllCurrencyTypes() {
    synchronized (currencyTypes) {
      if (currencyTypes.isEmpty()) {
        repository.findAll().stream()
            .map(c -> new CurrencyType(c.getCurrencyCode(), c.getCurrencyName()))
            .forEach(c -> currencyTypes.put(c.getCode(), c));
      }
    }
    return List.copyOf(currencyTypes.values());
  }

  @Override
  public Optional<CurrentCurrencyRate> getCurrentRateForCurrency(String code, String ip) {
    CurrencyPriceKey key = new CurrencyPriceKey(code, LocalDate.MAX);
    return historyRepository
        .findById(key)
        .map(
            c -> {
              Locale locale = localeService.getLocaleFromIP(ip);
              NumberFormat currencyInstance = DecimalFormat.getCurrencyInstance(locale);
              currencyInstance.setMaximumFractionDigits(4);
              return new CurrentCurrencyRate(
                  code, currencyTypes.get(code).getName(), currencyInstance.format(c.getPrice()));
            });
  }

  @Override
  public List<PriceHistoryEntry> getPriceHistory(String code, String ip) {
    Locale locale = localeService.getLocaleFromIP(ip);
    return historyRepository.findAllByGivenCode(code).stream()
        .filter(h -> h.getKey().getPriceDate().isBefore(LocalDate.now()))
        .map(h -> this.getPriceHistoryEntry(h, locale))
        .collect(Collectors.toList());
  }

  private PriceHistoryEntry getPriceHistoryEntry(CurrencyHistory history, Locale locale) {
    NumberFormat currencyInstance = DecimalFormat.getCurrencyInstance(locale);
    currencyInstance.setMaximumFractionDigits(4);
    DateTimeFormatter dateFormat =
        DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM).withLocale(locale);
    return new PriceHistoryEntry(
        dateFormat.format(history.getKey().getPriceDate()),
        currencyInstance.format(history.getPrice()));
  }
}
