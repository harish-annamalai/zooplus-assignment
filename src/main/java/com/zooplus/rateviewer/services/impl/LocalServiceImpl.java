package com.zooplus.rateviewer.services.impl;

import com.zooplus.rateviewer.entities.IP2CountryEntry;
import com.zooplus.rateviewer.repositories.IP2CountryRepository;
import com.zooplus.rateviewer.services.LocaleService;
import com.zooplus.rateviewer.utilities.IPUtilities;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.*;

@Service
public class LocalServiceImpl implements LocaleService {

  @Autowired IP2CountryRepository ip2CountryRepository;

  Map<String, Locale> countryToLocaleMap = new HashMap<>();

  @PostConstruct
  public void init() {
    Arrays.stream(Locale.getAvailableLocales())
        .forEach(
            l -> {
              String countyCode = l.getCountry().toUpperCase();
              if (!countryToLocaleMap.containsKey(countyCode)) {
                countryToLocaleMap.put(countyCode, l);
              } else {
                if (l.getLanguage().equalsIgnoreCase("EN")) {
                  countryToLocaleMap.put(countyCode, l);
                }
              }
            });
  }

  @Override
  public Locale getLocaleFromCountryCode(String countryCode) {
    return Optional.ofNullable(countryToLocaleMap.get(countryCode)).orElse(Locale.getDefault());
  }

  @Override
  public Locale getLocaleFromIP(String ip) {
    return Optional.ofNullable(
            ip2CountryRepository.findByGivenIP(IPUtilities.getIPInDecimalFormat(ip)))
        .map(IP2CountryEntry::getCountryCode)
        .map(this::getLocaleFromCountryCode)
        .orElse(Locale.getDefault());
  }
}
