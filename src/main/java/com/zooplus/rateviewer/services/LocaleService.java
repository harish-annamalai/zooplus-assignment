package com.zooplus.rateviewer.services;

import java.util.Locale;

public interface LocaleService {

  Locale getLocaleFromCountryCode(String countryCode);

  Locale getLocaleFromIP(String ip);
}
