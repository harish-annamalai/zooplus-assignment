package com.zooplus.rateviewer.services;

import com.zooplus.rateviewer.models.RateViewRequest;
import org.springframework.web.servlet.ModelAndView;

public interface ModelViewBuilderService {

  ModelAndView getViewForCurrentRateForRequest(RateViewRequest rateViewRequest);

  ModelAndView getLandingView();
}
