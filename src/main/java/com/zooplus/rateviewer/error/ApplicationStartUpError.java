package com.zooplus.rateviewer.error;

public class ApplicationStartUpError extends RuntimeException {
  private ApplicationStartUpError() {}

  public ApplicationStartUpError(String message) {
    super(message);
  }

  public ApplicationStartUpError(String message, Throwable t) {
    super(message, t);
  }
}
