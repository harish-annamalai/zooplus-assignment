package com.zooplus.rateviewer.configuration;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.zooplus.rateviewer.entities.Currencies;
import com.zooplus.rateviewer.entities.CurrencyHistory;
import com.zooplus.rateviewer.entities.CurrencyPriceKey;
import com.zooplus.rateviewer.entities.IP2CountryEntry;
import com.zooplus.rateviewer.error.ApplicationStartUpError;
import com.zooplus.rateviewer.models.CurrencyHistoryCSVObject;
import com.zooplus.rateviewer.repositories.CurrenciesRepository;
import com.zooplus.rateviewer.repositories.CurrencyHistoryRepository;
import com.zooplus.rateviewer.repositories.IP2CountryRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.EventListener;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Configuration
@Log4j2
@Profile("loadData")
public class StartUpConfiguration {

  @Autowired IP2CountryRepository ip2CountryRepository;
  @Autowired CurrenciesRepository currenciesRepository;
  @Autowired CurrencyHistoryRepository currencyHistoryRepository;

  @EventListener(classes = {ApplicationReadyEvent.class})
  public void handleStartUpEvents() {
    this.loadCurrencyTypes();
    this.loadIP2CountryDB();
    this.loadCurrencyHistory();
  }

  private void loadIP2CountryDB() {
    log.info("Loading IP2 Country Entries Started!");
    MappingIterator<IP2CountryEntry> entries = null;
    try {

      entries =
          new CsvMapper()
              .readerWithTypedSchemaFor(IP2CountryEntry.class)
              .readValues(
                  new InputStreamReader(
                      new ClassPathResource("ip2country.csv", this.getClass().getClassLoader())
                          .getInputStream()));
      entries.forEachRemaining(
          s -> {
            if (!s.getCountryCode().equals("-")) {
              ip2CountryRepository.save(s);
            }
          });
      log.info("Loading IP2 Country Entries Completed!");
    } catch (IOException e) {
      throw new ApplicationStartUpError("Failed to Load ip2country Database." + e.getMessage(), e);
    }
  }

  private void loadCurrencyTypes() {
    log.info("Loading Currency Types Started!");
    MappingIterator<Currencies> entries = null;
    try {
      entries =
          new CsvMapper()
              .readerWithTypedSchemaFor(Currencies.class)
              .readValues(
                  new ClassPathResource("currencies.csv", this.getClass().getClassLoader())
                      .getInputStream());
      entries.forEachRemaining(
          s -> {
            Currencies currency = new Currencies();
            currency.setCurrencyCode(s.getCurrencyCode());
            currency.setCurrencyName(s.getCurrencyName());
            currency.setCreatedDate(LocalDateTime.now());
            currency.setUpdatedDate(LocalDateTime.now());
            currenciesRepository.save(currency);
          });
      log.info("Loading Currency Types Completed!");
    } catch (Exception e) {
      throw new ApplicationStartUpError("Failed to Load Currencies Database." + e.getMessage(), e);
    }
  }

  private void loadCurrencyHistory() {
    log.info("Loading Currency History Started!");
    MappingIterator<CurrencyHistoryCSVObject> entries = null;
    try {
      entries =
          new CsvMapper()
              .readerWithTypedSchemaFor(CurrencyHistoryCSVObject.class)
              .readValues(
                  new ClassPathResource("currencyHistory.csv", this.getClass().getClassLoader())
                      .getInputStream());
      entries.forEachRemaining(
          s -> {
            CurrencyHistory history = new CurrencyHistory();
            CurrencyPriceKey key = new CurrencyPriceKey();
            key.setCurrencyCode(s.getCode());
            key.setPriceDate(
                s.getDate().equals("MAX") ? LocalDate.MAX : LocalDate.parse(s.getDate()));
            history.setKey(key);
            history.setPrice(new BigDecimal(s.getRate()));
            currencyHistoryRepository.save(history);
          });
      log.info("Loading Currency History Completed!");
    } catch (Exception e) {
      throw new ApplicationStartUpError(
          "Failed to Load Currencies History Database." + e.getMessage(), e);
    }
  }
}
