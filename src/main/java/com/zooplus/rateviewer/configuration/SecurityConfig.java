package com.zooplus.rateviewer.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
public class SecurityConfig {

  @Value("${defaultUser}")
  private String defaultUser;

  @Value("${defaultPassword}")
  private String defaultPassword;

  @Bean
  public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
    http.authorizeHttpRequests(
            (auth) ->
                auth.antMatchers("/css/*")
                    .permitAll()
                    .antMatchers("/")
                    .hasRole("user")
                    .anyRequest()
                    .authenticated())
        .formLogin()
        .loginPage("/login")
        .permitAll()
        .and()
        .logout()
        .permitAll();
    return http.build();
  }

  @Bean
  public UserDetailsManager users() {
    UserDetails user =
        User.withDefaultPasswordEncoder()
            .username(defaultUser)
            .password(defaultPassword)
            .roles("user", "moderator", "admin", "public")
            .build();
    return new InMemoryUserDetailsManager(user);
  }
}
