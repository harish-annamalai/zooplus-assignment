package com.zooplus.rateviewer.repositories;

import com.zooplus.rateviewer.entities.IP2CountryEntry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface IP2CountryRepository extends JpaRepository<IP2CountryEntry, Long> {

  @Query("select e from IP2CountryEntry e where ?1 >= e.startIP and ?1 <= e.endIP")
  IP2CountryEntry findByGivenIP(long ip);
}
