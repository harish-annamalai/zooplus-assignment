package com.zooplus.rateviewer.repositories;

import com.zooplus.rateviewer.entities.Currencies;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface CurrenciesRepository extends JpaRepository<Currencies, UUID> {

  Currencies findByCurrencyCode(String currencyCode);
}
