package com.zooplus.rateviewer.repositories;

import com.zooplus.rateviewer.entities.CurrencyHistory;
import com.zooplus.rateviewer.entities.CurrencyPriceKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CurrencyHistoryRepository
    extends JpaRepository<CurrencyHistory, CurrencyPriceKey> {

  @Query(
      "select e from CurrencyHistory e where e.key.currencyCode = ?1 order by e.key.priceDate desc")
  List<CurrencyHistory> findAllByGivenCode(String code);
}
