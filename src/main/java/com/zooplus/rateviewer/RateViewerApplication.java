package com.zooplus.rateviewer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RateViewerApplication {

  public static void main(String[] args) {
    SpringApplication.run(RateViewerApplication.class, args);
  }
}
