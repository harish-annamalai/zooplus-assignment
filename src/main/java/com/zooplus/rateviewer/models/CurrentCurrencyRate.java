package com.zooplus.rateviewer.models;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class CurrentCurrencyRate {
  @NonNull private String code;
  @NonNull private String name;
  @NonNull private String formattedCurrencyValue;
}
