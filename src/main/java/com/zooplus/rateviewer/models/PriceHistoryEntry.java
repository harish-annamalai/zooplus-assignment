package com.zooplus.rateviewer.models;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
@NoArgsConstructor
public class PriceHistoryEntry {
  @NonNull private String formattedDate;
  @NonNull private String formattedValue;
}
