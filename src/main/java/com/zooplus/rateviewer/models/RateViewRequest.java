package com.zooplus.rateviewer.models;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class RateViewRequest {
  private String ip;
  private String code;
}
