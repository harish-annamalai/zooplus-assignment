package com.zooplus.rateviewer.models;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class CurrencyRate {
  @NonNull private String code;
  @NonNull private BigDecimal currencyValue;
}
