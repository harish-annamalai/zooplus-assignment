package com.zooplus.rateviewer.models;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;
import lombok.ToString;

@Data
@JsonPropertyOrder({"code", "date", "rate"})
@ToString
public class CurrencyHistoryCSVObject {
  private String code;
  private String date;
  private String rate;
}
