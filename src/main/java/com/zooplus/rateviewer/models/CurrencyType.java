package com.zooplus.rateviewer.models;

import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class CurrencyType {
  @NonNull private String code;
  @NonNull private String name;
}
