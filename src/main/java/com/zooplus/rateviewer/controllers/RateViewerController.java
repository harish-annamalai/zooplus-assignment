package com.zooplus.rateviewer.controllers;

import com.zooplus.rateviewer.models.RateViewRequest;
import com.zooplus.rateviewer.services.ModelViewBuilderService;
import com.zooplus.rateviewer.utilities.IPUtilities;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
public class RateViewerController {

  @Autowired ModelViewBuilderService service;

  @GetMapping("/")
  public ModelAndView getLandingView() {
    return service.getLandingView();
  }

  @PostMapping("/getRates")
  public ModelAndView getPriceView(
      HttpServletRequest servletRequest,
      @ModelAttribute("rateViewRequest") RateViewRequest request) {
    if (request.getIp() == null || request.getIp().isEmpty()) {
      request.setIp(IPUtilities.getIPAddressFromServletRequest(servletRequest));
    }
    return service.getViewForCurrentRateForRequest(request);
  }
}
