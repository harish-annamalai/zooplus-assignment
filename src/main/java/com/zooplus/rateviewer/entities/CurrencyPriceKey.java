package com.zooplus.rateviewer.entities;

import lombok.*;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.time.LocalDate;

@Getter
@Setter
@Embeddable
@RequiredArgsConstructor
@NoArgsConstructor
public class CurrencyPriceKey implements Serializable {
  @NonNull String currencyCode;
  @NonNull LocalDate priceDate;
}
