package com.zooplus.rateviewer.entities;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Getter
@Setter
@JsonPropertyOrder({"currencyName", "currencyCode"})
public class Currencies implements Serializable {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private UUID id;

  private String currencyName;
  private String currencyCode;
  private LocalDateTime createdDate;
  private LocalDateTime updatedDate;
}
