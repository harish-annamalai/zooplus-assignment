package com.zooplus.rateviewer.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Getter
@Setter
public class CurrencyHistory implements Serializable {
  @EmbeddedId private CurrencyPriceKey key;
  private BigDecimal price;
}
