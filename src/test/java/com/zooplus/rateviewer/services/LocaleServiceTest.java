package com.zooplus.rateviewer.services;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Locale;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
public class LocaleServiceTest {

  @Autowired LocaleService localeService;

  @Test
  public void WhenGivenIP_And_IPisValid_ReturnAValidLocale() {
    Locale l = localeService.getLocaleFromIP("18.184.45.226"); // I.P Address in Germany.
    assertNotNull(l);
    assertEquals(Locale.GERMANY.getCountry(), l.getCountry());
  }

  @Test
  public void WhenGivenCountry_And_CountryISKnown_ReturnAValidLocale() {
    Locale l = localeService.getLocaleFromCountryCode("FR"); // France
    assertNotNull(l);
    assertEquals(Locale.FRANCE.getCountry(), l.getCountry());
  }
}
