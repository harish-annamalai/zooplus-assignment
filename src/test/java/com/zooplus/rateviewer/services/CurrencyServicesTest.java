package com.zooplus.rateviewer.services;

import com.zooplus.rateviewer.models.PriceHistoryEntry;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class CurrencyServicesTest {

  @Autowired CurrencyServices services;

  @Test
  public void FetchAllSupportedCryptoCurrency() {
    var currencies = services.getAllCurrencyTypes();
    assertNotNull(services.getAllCurrencyTypes());
    assertFalse(currencies.isEmpty());
  }

  @Test
  public void GivenValidCodeAndIP_AndDataExists_ReturnValidCurrencyRate() {
    var rate = services.getCurrentRateForCurrency("BTC", "18.194.155.22").orElse(null);
    assertNotNull(rate);

    assertEquals("BTC", rate.getCode());
    assertEquals("Bitcoin", rate.getName());
    assertTrue(rate.getFormattedCurrencyValue().matches("\\d+\\.\\d+,\\d+.€"));
  }

  @Test
  public void GivenValidCodeAndIP_AndDataExists_ReturnValidPriceHistory() {
    var entry = services.getPriceHistory("DOGE", "9.16.0.0");
    assertNotNull(entry);
    assertFalse(entry.isEmpty());

    PriceHistoryEntry test = entry.get(5);

    assertTrue(test.getFormattedValue().matches("\\$\\d+\\.\\d+"));
    assertTrue(test.getFormattedDate().matches("[a-zA-Z]{3} \\d{2}, \\d{4}"));
  }
}
