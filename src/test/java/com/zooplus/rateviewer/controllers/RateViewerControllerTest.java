package com.zooplus.rateviewer.controllers;

import org.hamcrest.core.StringStartsWith;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
@WithMockUser(
    username = "user",
    roles = {"user"})
public class RateViewerControllerTest {
  @Autowired MockMvc mockMvc;

  @Test
  public void getLandingPageTest() throws Exception {
    mockMvc
        .perform(MockMvcRequestBuilders.get("/"))
        .andExpect(status().isOk())
        .andExpect(view().name("index"))
        .andExpect(model().attribute("errorText", ""));
  }

  @Test
  public void getRatesTest() throws Exception {
    mockMvc
        .perform(
            MockMvcRequestBuilders.post("/getRates")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .content("code=BTC&ip=123.45.45.0")
                .with(csrf()))
        .andExpect(status().isOk())
        .andExpect(view().name("index"))
        .andExpect(model().attribute("errorText", ""))
        .andExpect(model().attribute("responseText", new StringStartsWith(true, "Price of")));
  }
}
