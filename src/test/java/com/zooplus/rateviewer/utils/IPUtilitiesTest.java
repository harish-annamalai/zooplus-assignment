package com.zooplus.rateviewer.utils;

import com.zooplus.rateviewer.utilities.IPUtilities;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import javax.servlet.http.HttpServletRequest;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class IPUtilitiesTest {

  @BeforeEach
  public void getIPAddressFromServletRequest() {}

  @Test
  public void testGetIPInDecimalFormat() {
    assertEquals(
        2130706433L,
        IPUtilities.getIPInDecimalFormat("127.0.0.1"),
        "I.P 2 Decimal Conversion failed");
    assertEquals(
        3418422192L,
        IPUtilities.getIPInDecimalFormat("203.192.251.176"),
        "I.P 2 Decimal Conversion failed");
    assertEquals(
        16909060, IPUtilities.getIPInDecimalFormat("1.2.3.4"), "I.P 2 Decimal Conversion failed");
    assertEquals(
        16843009, IPUtilities.getIPInDecimalFormat("1.1.1.1"), "I.P 2 Decimal Conversion failed");
  }

  @Test
  public void testGetIPAddressFromServletRequest_Header() {
    HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
    Mockito.when(request.getHeader("X-Forwarded-For")).thenReturn("1.1.1.1");
    assertEquals("1.1.1.1", IPUtilities.getIPAddressFromServletRequest(request));
  }

  @Test
  public void testGetIPAddressFromServletRequest_RequestURL() {
    HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
    Mockito.when(request.getRequestURL()).thenReturn(new StringBuffer("localhost"));
    assertEquals("127.0.0.1", IPUtilities.getIPAddressFromServletRequest(request));
  }

  @Test
  public void testGetIPAddressFromServletRequest_RemoteAddr() {
    HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
    Mockito.when(request.getRequestURL()).thenReturn(new StringBuffer("remoteHost"));
    Mockito.when(request.getRemoteAddr()).thenReturn("1.2.3.4");
    assertEquals("1.2.3.4", IPUtilities.getIPAddressFromServletRequest(request));
  }
}
